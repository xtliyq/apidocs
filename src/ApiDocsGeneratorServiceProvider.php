<?php
/**
 * Created by PhpStorm.
 * User: xtliy
 * Date: 2018/4/24
 * Time: 16:45
 */

namespace Liyq\ApiDocs;

use Illuminate\Support\ServiceProvider;
use Liyq\ApiDocs\Console\GenerateDocumentation;

class ApiDocsGeneratorServiceProvider extends ServiceProvider
{
    public function boot() {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'apidocs');

        $this->publishes([
            __DIR__ . '/../resources/views' => $this->app->basePath() . '/resources/views/vendor/apidocs'
        ]);
    }

    /**
     * 注册服务
     */
    public function register() {
        $this->app->singleton('api.generate', function () {
            return new GenerateDocumentation();
        });
        $this->commands(['api.generate']);
    }
}